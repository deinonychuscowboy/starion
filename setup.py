#!/usr/bin/env python3
from distutils.core import setup
from Cython.Build import cythonize
from glob import glob
import multiprocessing,shutil

setup(
	ext_modules=cythonize("starion/data/*.py",None,multiprocessing.cpu_count()),
	include_dirs="starion/"
)
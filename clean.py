#!/usr/bin/env python3

import os,shutil
from glob import glob

for file in glob("./starion/data/*.c"):
	os.remove(file)

for file in glob("./starion/data/*.so"):
	os.remove(file)

shutil.rmtree("build")
#!/usr/bin/env python3

import cProfile

from data.map import Map
from data.generate import MapGenerator
from data.display import DisplayEngine

cProfile.run("""
map=Map()
generator=MapGenerator(map)
generator.stage1()
generator.stage2()
generator.stage3()
displayengine=DisplayEngine(map)
displayengine.display("nucleation","none")
displayengine.display("continents","none")
displayengine.display("basins","none")
displayengine.display("realistic","elevation")
displayengine.display("temperature","distance_from_saltwater")
displayengine.display("moisture","prevailing_wind")
displayengine.display("moisture","oscillating_wind")
""")
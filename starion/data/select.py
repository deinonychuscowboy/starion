# cython: language_level=3
from typing import *
from data.map import Map,Point
from data.types import *
import random

class Selector:
	__last_random_walk_direction=0

	def __init__(self,map:Map):
		self.__map=map

	def select_random(self, set:Union[List,Tuple])->float:
		return set[random.randint(0,len(set)-1)]

	def select_weighted_random_walk(self,point:Point)->Point:
		total_distance=0
		total_probability=0
		distances=[]
		probabilities=[]
		neighbors=point.neighbors
		for neighbor in neighbors:
			distance=self.__map.compute_real_distance(point,neighbor)
			total_distance+=distance
			distances.append((distance,neighbor))
		for neighbor in distances:
			total_probability+=total_distance/neighbor[0]
			probabilities.append((total_distance/neighbor[0],neighbor[1]))
		random_value=random.random()*total_probability
		total_probability=0
		for neighbor in probabilities:
			total_probability+=neighbor[0]
			if random_value<=total_probability:
				return neighbor[1]

	def select_random_walk(self, point:Point, stickiness:float=0)->Point:
		# do not try to maintain direction if this is a polar point, we need to select a random one of the many neighbors
		if stickiness!=0 and random.random()<stickiness and not point.is_pole:
			points=point.neighbors
			return points[Selector.__last_random_walk_direction%len(points)]
		elif stickiness!=0 and random.random()<stickiness and not point.is_pole:
			points=point.neighbors
			Selector.__last_random_walk_direction+=(1 if random.random()<0.5 else -1)
			return points[Selector.__last_random_walk_direction%len(points)]
		else:
			# prefer neighbors that are closer to the given point, i.e. cardinal points in general and e/w points at high latitudes
			Selector.__last_random_walk_direction=random.randint(0,7)
			return self.select_weighted_random_walk(point)

	def select_random_sea(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain == TerrainType.LAND:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_random_land(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain != TerrainType.LAND:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_random_sea_noncoast(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain == TerrainType.LAND or len(tuple(x for x in self.__map[coords].neighbors if x.terrain==TerrainType.LAND))>0:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_random_sea_coast(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain == TerrainType.LAND or len(tuple(x for x in self.__map[coords].neighbors if x.terrain==TerrainType.LAND))==0:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_random_land_noncoast(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain != TerrainType.LAND or len(tuple(x for x in self.__map[coords].neighbors if x.terrain!=TerrainType.LAND))>0:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_random_land_coast(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain != TerrainType.LAND or len(tuple(x for x in self.__map[coords].neighbors if x.terrain!=TerrainType.LAND))==0:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_random_land_highlands(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain != TerrainType.LAND or self.__map[coords].elevation != ElevationType.HIGHLANDS:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_random_land_lowlands(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain != TerrainType.LAND or self.__map[coords].elevation != ElevationType.LOWLANDS:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_random_land_plains(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain != TerrainType.LAND or self.__map[coords].elevation != ElevationType.PLAINS:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_random_land_mountains(self)->Point:
		coords:Tuple[float,float]=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		while self.__map[coords].terrain != TerrainType.LAND or self.__map[coords].elevation != ElevationType.MOUNTAINS:
			coords=(self.select_random(self.__map.get_latitude_cells()), self.select_random(self.__map.get_longitude_cells()))
		return self.__map[coords]

	def select_walk_diagonal(self,point:Point)->Tuple[Point,...]:
		return tuple({self.__map[point.X + self.__map.precision, point.Y+self.__map.precision],self.__map[point.X - self.__map.precision, point.Y+self.__map.precision],self.__map[point.X - self.__map.precision, point.Y-self.__map.precision],self.__map[point.X + self.__map.precision, point.Y-self.__map.precision]})

	def select_walk_cardinal(self, point: Point) -> Tuple[Point, ...]:
		return tuple({self.__map[point.X + self.__map.precision, point.Y], self.__map[point.X, point.Y + self.__map.precision], self.__map[point.X - self.__map.precision, point.Y],  self.__map[point.X, point.Y - self.__map.precision]})
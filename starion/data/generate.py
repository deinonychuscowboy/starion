# cython: language_level=3
from typing import *
from data import *
from data.select import Selector
from data.types import *
from data.map import Map
from data.generates1 import GenerateS1
from data.generates2 import GenerateS2
from data.generates3 import GenerateS3

class MapGenerator:
	def __init__(self,map:Map):
		self.__map=map
		self.__selector=Selector(map)
		self._progress_total:float=0
		self._progress_amount:float=0
		self._progress_last_str_len:int=0

	def initialize_progress(self,total:float):
		self._progress_total=total
		self._progress_amount=0
		self._progress_last_str_len=0
	def update_progress(self,amount:float):
		print("\b"*self._progress_last_str_len,end="")
		pstr=str(int(self._progress_amount/self._progress_total*100))+"%"
		self._progress_last_str_len=len(pstr)
		print(pstr,end="",flush=True)
		self._progress_amount+=amount
	def clear_progress(self):
		print("\b"*self._progress_last_str_len,end="")
		self.initialize_progress(0)

	def getpointkey(self,x):
		map=self.__map
		return (x.Y+map.get_latitude_span()/2.0)*map.get_longitude_span()+(x.X+map.get_longitude_span()/2.0)

	def compute_skewfactor(self,rate:float,mult:float,metric_northward:float,metric_southward:float,offset:float)->float:
		computed_skewrate=rate/self.__map.precision
		return (1+metric_northward*computed_skewrate*mult*2)/(1+metric_southward*(computed_skewrate+offset)*mult)

	def tempslur(self):
		for point in self.__map.get_points(TerrainType.LAND):
			if point.temperature==point.neighbors[DirectionType.NORTHEAST].temperature:
				if point.temperature<point.neighbors[DirectionType.NORTH].temperature:
					point.neighbors[DirectionType.NORTH].temperature-=1
				if point.temperature>point.neighbors[DirectionType.EAST].temperature:
					point.neighbors[DirectionType.EAST].temperature+=1
			if point.temperature==point.neighbors[DirectionType.NORTHWEST].temperature:
				if point.temperature<point.neighbors[DirectionType.NORTH].temperature:
					point.neighbors[DirectionType.NORTH].temperature-=1
				if point.temperature>point.neighbors[DirectionType.WEST].temperature:
					point.neighbors[DirectionType.WEST].temperature+=1

	def stage1(self):
		GenerateS1(self,self.__map,self.__selector).run()

	def stage2(self):
		GenerateS2(self,self.__map,self.__selector).run()

	def stage3(self):
		GenerateS3(self,self.__map,self.__selector).run()

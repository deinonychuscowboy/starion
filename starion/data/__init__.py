# cython: language_level=3
from typing import *
import resource, sys

# unlimit the stack, used for some wannabe-functional arguments
resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY))
sys.setrecursionlimit(int(sys.maxsize/2**32)-1)

# works like stock range function, start is inclusive, end is exclusive
def frange(start: float,end: float,precision: float=1):
	val: float=start
	yield val
	while val+precision<end:
		val+=precision
		yield val
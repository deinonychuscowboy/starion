# cython: language_level=3
from data import *
from data.select import Selector
from data.types import *
from data.map import Map

class GenerateS3:
	"""
	Temperature, winds, and climate
	"""
	def __init__(self,generator:'MapGenerator',map:Map,selector:Selector):
		self.__generator:'MapGenerator'=generator
		self.__map:Map=map
		self.__selector:Selector=selector

	def run(self):
		# temperature banding
		# Tropics are between 23N and 23S (23/90)
		# Ant/Arctic circle is at 66N and 66S (66/90)
		# Space between is split evenly between cold/cool/temperate/warm
		# band lines are drawn down in continental areas and drawn up in water areas, draw is more aggressive for warmer temperatures than colder
		# TODO it would be NICE if we could do some really basic thermohaline circulation / ocean gyre faking similar to the temperature slur system but on a larger scale. I.e. if there's a sizeable ocean basin it will transport heat clockwise in the northern hemisphere and counterclockwise in the southern hemisphere, so heat dynamics around it will have a pronounced northeasterly heat stripe in the northern hemisphere and southwesterly in the southern hemisphere. Could maybe even do this without having basins by just looking at how many diagonal water tiles there are between a square and the equator vs. how many land tiles
		print("Calculating temperatures...")
		self.__temperatures()

		print("Calculating prevailing winds...")
		self.__prevailing_winds()

		print("Calculating oscillating winds...")
		# TODO this requires identifying ocean basins

	def __temperatures(self):
		self.__compute_temp_bands()
		# temperature slurring: diagonal relationships are slurred
		self.__generator.tempslur()
		# temperature drawdown: All mountains get a colder temperature than they have currently
		for point in self.__map.get_points(ElevationType.MOUNTAINS):
			# weird neighbors check here is just to allow disabling all drawing by setting neighborhood to 9
			if point.temperature>TempType.POLAR and len(point.neighbors)>=self.__map.temperature_draw_neighborhood:
				point.temperature-=1
		# temperature slurring: diagonal relationships are slurred
		self.__generator.tempslur()
		for run in frange(0,self.__map.elevation_temperature_factor):
			# temperature drawdown: If a highlands is bordered by at least five cooler regions than itself, it cools as well
			for point in self.__map.get_points(ElevationType.HIGHLANDS):
				colder_neighbors=tuple(
					neighbor for neighbor in point.neighbors if neighbor.temperature<point.temperature)
				if len(colder_neighbors)>=self.__map.temperature_draw_neighborhood and point.temperature>TempType.POLAR:
					point.temperature-=1
			# temperature drawup: If a lowlands is bordered by at least five warmer regions than itself or three warmer regions and one oceanic region, it warms as well
			for point in self.__map.get_points(ElevationType.LOWLANDS):
				warmer_neighbors=tuple(
					neighbor for neighbor in point.neighbors if neighbor.temperature>point.temperature)
				water_neighbors=tuple(
					neighbor for neighbor in point.neighbors if neighbor.terrain==TerrainType.SALTWATER)
				if (len(warmer_neighbors)>=self.__map.temperature_draw_neighborhood or len(
						warmer_neighbors)>=self.__map.temperature_draw_neighborhood/2.0 and len(
						water_neighbors)>=1 and point.temperature>TempType.POLAR) and point.temperature<TempType.HOT:
					point.temperature+=1
		# temperature slurring: diagonal relationships are slurred
		self.__generator.tempslur()
		# band cleanup - forbid points of non-neighboring bands from touching
		changed_points=1
		while changed_points>0:
			changed_points=0
			for point in self.__map.get_points():
				for neighbor in point.neighbors:
					if point.temperature-neighbor.temperature>=2:
						changed_points+=1
						point.temperature-=(point.temperature-neighbor.temperature-1)

	def __compute_temp_bands(self):
		for point in self.__map.get_points():
			# tropic of cancer is 23.43683 repeating, arctic circle is 66.56316 repeating, constants are computed based on earth coordinates but applicable to any planet after conversion
			# (i.e. the 180s below should NOT use latitude_span since 23.43683 is earth-specific)
			if self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/66.5631666667))*self.__generator.compute_skewfactor(
						self.__map.temperature_skewrate,self.__map.temperature_skewmult,point.distance_from_land,
						point.distance_from_saltwater,0),
					self.__map.temperature_multiplier):
				point.temperature=TempType.POLAR
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/55.7815833333))*self.__generator.compute_skewfactor(
						self.__map.temperature_skewrate,self.__map.temperature_skewmult,point.distance_from_land,
						point.distance_from_saltwater,0.025),
					self.__map.temperature_multiplier):
				point.temperature=TempType.COLD
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/45))*self.__generator.compute_skewfactor(
						self.__map.temperature_skewrate,
						self.__map.temperature_skewmult,point.distance_from_land,point.distance_from_saltwater,0.05),
					self.__map.temperature_multiplier):
				point.temperature=TempType.COOL
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/34.2184166667))*self.__generator.compute_skewfactor(
						self.__map.temperature_skewrate,self.__map.temperature_skewmult,point.distance_from_land,
						point.distance_from_saltwater,0.1),
					self.__map.temperature_multiplier):
				point.temperature=TempType.TEMPERATE
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/23.4368333333))*self.__generator.compute_skewfactor(
						self.__map.temperature_skewrate,self.__map.temperature_skewmult,point.distance_from_land,
						point.distance_from_saltwater,0.2),
					self.__map.temperature_multiplier):
				point.temperature=TempType.WARM
			else:
				point.temperature=TempType.HOT

	def __prevailing_winds(self):
		# wind banding
		# ICT is located at 0 and is 10* wide
		# Trade winds are between 30N and 0 and are driven by hadley cell mechanics
		# Horse latitudes are located at 30N and are 5* wide
		# Westerlies are between 60N and 30N and are driven by ferrel cell mechanics
		# Polar front is located at 60N and is 5* wide
		# Polar vortex winds are between 90N and 60N and are driven by polar cell mechanics
		for point in self.__map.get_points():
			# opposite to the direction that temperature skews
			# TODO right now skewfactor operates symmetrically, i.e. the equator will not move and each hemisphere is computed relative to it. This is right for temperature (the hottest region is always a stationary band centered on the equator and between the tropics) but not for winds, the ICT should be able to move north/south just like other areas (it moves north when there is land north of it, south when there is land south of it, in addition to the normal seasonal move toward whichever hemisphere is having summer). This is not a huge problem, so not a priority right now.
			skewfactor=self.__generator.compute_skewfactor(self.__map.wind_skewrate,self.__map.wind_skewmult,
				point.distance_from_saltwater,point.distance_from_land,0)
			if point.Y==self.__map.get_latitude_start() or point.Y==self.__map.get_latitude_end() or point in self.__map.get_polar_ring_points():
				# pole
				point.prevailing_wind=LatitudinalWindType.DOLDRUMS
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/82))*skewfactor,
					self.__map.wind_multiplier):
				# polar cell descending
				if point.latitudinal_hemisphere==DirectionType.NORTH:
					point.prevailing_wind=LatitudinalWindType.SOUTH
				elif point.latitudinal_hemisphere==DirectionType.SOUTH:
					point.prevailing_wind=LatitudinalWindType.NORTH
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/73))*skewfactor,
					self.__map.wind_multiplier):
				# polar cell base
				if point.latitudinal_hemisphere==DirectionType.NORTH:
					point.prevailing_wind=LatitudinalWindType.SOUTHWEST
				elif point.latitudinal_hemisphere==DirectionType.SOUTH:
					point.prevailing_wind=LatitudinalWindType.NORTHWEST
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/65))*skewfactor,
					self.__map.wind_multiplier):
				# polar cell ascending
				point.prevailing_wind=LatitudinalWindType.WEST
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/60))*skewfactor,
					self.__map.wind_multiplier):
				# polar front
				point.prevailing_wind=LatitudinalWindType.DOLDRUMS
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/52))*skewfactor,
					self.__map.wind_multiplier):
				# ferrel cell ascending
				if point.latitudinal_hemisphere==DirectionType.NORTH:
					point.prevailing_wind=LatitudinalWindType.NORTH
				elif point.latitudinal_hemisphere==DirectionType.SOUTH:
					point.prevailing_wind=LatitudinalWindType.SOUTH
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/43))*skewfactor,
					self.__map.wind_multiplier):
				# ferrel cell base
				if point.latitudinal_hemisphere==DirectionType.NORTH:
					point.prevailing_wind=LatitudinalWindType.NORTHEAST
				elif point.latitudinal_hemisphere==DirectionType.SOUTH:
					point.prevailing_wind=LatitudinalWindType.SOUTHEAST
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/35))*skewfactor,
					self.__map.wind_multiplier):
				# ferrel cell descending
				point.prevailing_wind=LatitudinalWindType.EAST
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/30))*skewfactor,
					self.__map.wind_multiplier):
				# horse latitudes
				point.prevailing_wind=LatitudinalWindType.DOLDRUMS
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/22))*skewfactor,
					self.__map.wind_multiplier):
				# trade winds descending
				if point.latitudinal_hemisphere==DirectionType.NORTH:
					point.prevailing_wind=LatitudinalWindType.SOUTH
				elif point.latitudinal_hemisphere==DirectionType.SOUTH:
					point.prevailing_wind=LatitudinalWindType.NORTH
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/13))*skewfactor,
					self.__map.wind_multiplier):
				# trade winds base
				if point.latitudinal_hemisphere==DirectionType.NORTH:
					point.prevailing_wind=LatitudinalWindType.SOUTHWEST
				elif point.latitudinal_hemisphere==DirectionType.SOUTH:
					point.prevailing_wind=LatitudinalWindType.NORTHWEST
			elif self.__map.equivalent_latitude(point,
					(self.__map.get_latitude_span()/(180/5))*skewfactor,
					self.__map.wind_multiplier):
				# trade winds ascending
				point.prevailing_wind=LatitudinalWindType.WEST
			else:
				# ICT -- intentionally larger than other doldrums to reflect earth reality
				point.prevailing_wind=LatitudinalWindType.DOLDRUMS

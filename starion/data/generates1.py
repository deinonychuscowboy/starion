# cython: language_level=3
from data import *
from data.select import Selector
from data.types import *
from data.map import Map,Point
import random

class GenerateS1:
	"""
	Basic terrain generation stuffs
	"""
	def __init__(self,generator:'MapGenerator',map:Map,selector:Selector):
		self.__generator:'MapGenerator'=generator
		self.__map:Map=map
		self.__selector:Selector=selector

		self.__continent_nucleation_points: List[Point]=[]
		self.__continent_sizes: List[int]=[]

	def __expand_continent(self, continent:int, continent_nucleation_point:Point,continent_walk_starting_point:Point,variance:float)->Point:
		point: Point = continent_walk_starting_point
		# walk to the next sea cell
		while point.terrain == TerrainType.LAND:
			point = self.__selector.select_random_walk(point, variance)
		# change it to a land cell and associate it with the current continent
		point.terrain = TerrainType.LAND
		point.nucleation_point=continent
		# this continent is now walking from the current point, or from the nucleation point depending on how much variance there is
		continent_walk_starting_point=point
		if random.random() > variance:
			continent_walk_starting_point=continent_nucleation_point
		return continent_walk_starting_point

	def __expand_continents_pass(self,continent_nucleation_points,continent_sizes,variance,fraction):
		map=self.__map
		continent_walk_starting_points=continent_nucleation_points
		# we calculate the desired land/sea ratio to build up to in this pass, including the fraction argument
		# total ratio is the total landmass amount up to the fraction we're doing in this pass -- all land, PLUS all freshwater lakes (which will occupy land area), MINUS all islands (which will occupy sea area)
		total_ratio=(map.land_sea_ratio+(
				1-map.land_sea_ratio)*map.freshwater_multiplier-map.land_sea_ratio*map.island_continents_ratio)*fraction
		# however some of this landmass may have already been built in previous passes, so we subtract the current ratio
		land_area=map.aggregate_area(map.get_points(TerrainType.LAND))
		water_area=map.aggregate_area(map.get_points(TerrainType.INCOGNITUM))
		current_ratio=land_area/(water_area+land_area)
		# the desired ratio is the portion of the ratio that will be built up in this pass
		desired_ratio=total_ratio-current_ratio
		# we can then determine how many cells need to be changed for each continent for the overall ratio to reach the desired ratio
		magnitude=map.get_magnitude()
		runs:Tuple[int]=[int(x*magnitude*desired_ratio) for x in continent_sizes]
		total_runs=sum(x for x in runs)
		self.__generator.initialize_progress(total_runs)
		# to allow continents to interact with each other as they build, we don't build all of one continent, then all of the second, etc, instead we use a quantizer to rotate over the list until all runs are complete
		quantizer=-1
		continent_count=map.continent_nucleation_points
		# run the total number of times needed to complete all runs
		for i in range(total_runs):
			# rotate to the next quantized position that needs a run
			quantizer+=1
			quantizer%=continent_count
			while runs[quantizer]==0:
				quantizer+=1
				quantizer%=continent_count
			self.__generator.update_progress(1)
			# subtract the current run from the run plan
			runs[quantizer]-=1
			# expand the continent at this location
			continent_walk_starting_points[quantizer]=self.__expand_continent(quantizer,continent_nucleation_points[quantizer],continent_walk_starting_points[quantizer],variance)
		self.__generator.clear_progress()

	def run(self):
		print("Setting up map...")
		self.__map.initialize()

		print("Selecting landmass nucleation points...")
		self.__select_nucleation_points()

		print("Generating landmasses...")
		self.__generate_landmasses()

		print("Cleaning up landmasses...")
		self.__cleanup_landmasses()

		# TODO these should both probably be rewritten to work like continent generation, with defined decreasing sizes and AOT nonrandom computation. Islands needs a concept of "island clusters" to better represent reality. However, the current version doesn't look super bad, unlike the old continent generation, so not a huge priority
		print("Generating islands...")
		self.__generate_islands_old()
		print("Generating lakes...")
		self.__generate_lakes_old()

		print("Recentering map...")
		self.__recenter_map()

	def __recenter_map(self):
		ocean_columns=[]
		column_counter=0
		for x in self.__map.get_latitude_cells():
			column_sum: int=0
			for y in self.__map.get_longitude_cells():
				if y not in (
						self.__map.get_latitude_start(),
						self.__map.get_latitude_end()) or x==self.__map.get_longitude_start():
					column_sum+=(1 if self.__map[x,y].terrain!=TerrainType.SALTWATER else 0)
			ocean_columns.append(column_sum)
			column_counter+=1
		min_ocean_columns=self.__map.get_latitude_span()/self.__map.precision*2+1
		rightward_shift=0
		for index in range(0,column_counter):
			if ocean_columns[index]+ocean_columns[(index+1)%column_counter]<=min_ocean_columns:
				min_ocean_columns=ocean_columns[index]+ocean_columns[(index+1)%column_counter]
				rightward_shift=index*self.__map.precision
		self.__map.recenter(rightward_shift)

	def __generate_lakes_old(self):
		last_lake: Point=self.__selector.select_random_land_noncoast()
		done=False
		while not done:
			land_area=self.__map.aggregate_area(self.__map.get_points(TerrainType.LAND))
			water_area=self.__map.aggregate_area(
				self.__map.get_points(TerrainType.SALTWATER)+self.__map.get_points(TerrainType.FRESHWATER))
			current_ratio=land_area/(water_area+land_area)
			desired_ratio=self.__map.land_sea_ratio
			done=current_ratio<=desired_ratio
			if random.random()>self.__map.lake_size_multiplier:
				last_lake=self.__selector.select_random_land_noncoast()
			else:
				while last_lake.terrain!=TerrainType.LAND or len(
						tuple(x for x in last_lake.neighbors if x.terrain==TerrainType.SALTWATER))>0:
					last_lake=self.__selector.select_random_walk(last_lake)
			last_lake.terrain=TerrainType.FRESHWATER

	def __generate_islands_old(self):
		last_island: Point=self.__selector.select_random_sea_noncoast()
		done=False
		while not done:
			land_area=self.__map.aggregate_area(self.__map.get_points(TerrainType.LAND))
			water_area=self.__map.aggregate_area(self.__map.get_points(TerrainType.SALTWATER))
			current_ratio=land_area/(water_area+land_area)
			desired_ratio=self.__map.land_sea_ratio+(1-self.__map.land_sea_ratio)*self.__map.freshwater_multiplier
			done=current_ratio>=desired_ratio
			if random.random()>self.__map.island_size_multiplier:
				last_island=self.__selector.select_random_sea_noncoast()
			else:
				while last_island.terrain==TerrainType.LAND or len(tuple(x for x in last_island.neighbors if
						x.terrain==TerrainType.LAND and x.nucleation_point!=self.__map.continent_nucleation_points))>0:
					last_island=self.__selector.select_random_walk(last_island)
			last_island.terrain=TerrainType.LAND
			last_island.nucleation_point=self.__map.continent_nucleation_points

	def __cleanup_landmasses(self):
		counter=self.__map.get_magnitude()
		continent_cleanup_size=self.__map.continent_cleanup_resolution/self.__map.precision
		#TODO this should all be broken up/made reusable, probably even in MapGenerator
		# build ancestor trees for all sea points
		# must sort points to ensure they are enumerated in order and every point can see at least one potential ancestor
		for point in sorted(self.__map.get_points(TerrainType.SALTWATER),key=self.__generator.getpointkey):
			counter-=1
			point.age=counter
			ancestors=tuple(x.ancestor for x in point.neighbors if x.ancestor is not None)
			if len(ancestors)>0:
				greatest_ancestor=ancestors[0]
				for ancestor in ancestors:
					if ancestor.age>greatest_ancestor.age:
						greatest_ancestor=ancestor
				point.ancestor=greatest_ancestor
			else:
				point.ancestor=point
		# determine how large certain saltwater areas are by telling all sea points to give their topmost ancestor their weight
		for point in self.__map.get_points(TerrainType.SALTWATER):
			ancestor=point
			while ancestor.ancestor!=ancestor:
				ancestor=ancestor.ancestor
			ancestor.weight+=1
		# for all saltwater points, if their topmost ancestor does not have enough weight, make the point land instead to eliminate the tiny saltwater lake
		for point in self.__map.get_points(TerrainType.SALTWATER):
			ancestor=point
			while ancestor.ancestor!=ancestor:
				ancestor=ancestor.ancestor
			if ancestor.weight<continent_cleanup_size:
				point.terrain=TerrainType.LAND
				point.nucleation_point=self.__map.continent_nucleation_points
		# rebalance the land-sea ratio by removing "safe" coastal land points
		done=False
		while not done:
			land_area=self.__map.aggregate_area(self.__map.get_points(TerrainType.LAND))
			water_area=self.__map.aggregate_area(self.__map.get_points(TerrainType.SALTWATER))
			current_ratio=land_area/(water_area+land_area)
			desired_ratio=(self.__map.land_sea_ratio+(
					1-self.__map.land_sea_ratio)*self.__map.freshwater_multiplier-self.__map.land_sea_ratio*self.__map.island_continents_ratio)
			done=current_ratio<=desired_ratio
			point=self.__selector.select_random_land_coast()
			point.terrain=TerrainType.SALTWATER
			point.nucleation_point=-1

	def __generate_landmasses(self):
		# build up the continent masses, see __expand_continent methods for more
		for i in range(len(self.__map.continent_passes)):
			print("Pass "+str(i+1)+"/"+str(len(self.__map.continent_passes))+"...")
			self.__expand_continents_pass(self.__continent_nucleation_points,self.__continent_sizes,
				self.__map.continent_passes[i][0],
				self.__map.continent_passes[i][1])
		# everything that is not land is now sea
		for point in self.__map.get_points(TerrainType.INCOGNITUM):
			point.terrain=TerrainType.SALTWATER

	def __select_nucleation_points(self):
		for index in frange(0,self.__map.continent_nucleation_points):
			# find random points to situate continents on
			coords: Tuple[float,float]=(
				self.__selector.select_random(self.__map.get_latitude_cells()),self.__selector.select_random(
					self.__map.get_longitude_cells()))
			point: Point=self.__map[coords]
			self.__continent_nucleation_points.append(point)
			# give each continent nucleation point a randomly assigned size based on the specified multiplier (triangular distribution; multiplier of 1 = fully random sizes,  multiplier of 0 = relatively uniform sizes)
			self.__continent_sizes.append(random.triangular(0,1,1-(self.__map.continent_multiplier/2.0)))
			point.terrain=TerrainType.LAND
			point.nucleation_point=index
		landmass_total_size=sum(x for x in self.__continent_sizes)
		self.__continent_sizes=[x/landmass_total_size for x in self.__continent_sizes]

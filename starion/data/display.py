# cython: language_level=3
from data import frange
from data.types import *
from data.map import Map,Point

class DisplayEngine:
	def __init__(self,map:Map):
		self.__map=map
	def _color_temp(self,point:Point):
		color='\033[0m'
		if point.temperature==TempType.POLAR:
			color='\033[95m'
		if point.temperature==TempType.COLD:
			color='\033[94m'
		if point.temperature==TempType.COOL:
			color='\033[96m'
		if point.temperature==TempType.TEMPERATE:
			color='\033[92m'
		if point.temperature==TempType.WARM:
			color='\033[93m'
		if point.temperature==TempType.HOT:
			color='\033[91m'
		return color

	def _color_moisture(self,point:Point):
		color='\033[0m'
		if point.moisture==MoistureType.WET:
			color='\033[96m'
		if point.moisture==MoistureType.AVERAGE:
			color='\033[92m'
		if point.moisture==MoistureType.DRY:
			color='\033[93m'
		return color

	def _color_identify_metric(self,metric:int):
		color='\033[30m'
		colors=(
			'\033[91m',
			'\033[92m',
			'\033[93m',
			'\033[94m',
			'\033[95m',
			'\033[96m',
			'\033[31m',
			'\033[32m',
			'\033[33m',
			'\033[34m',
			'\033[35m',
			'\033[36m',
			'\033[37m',
			'\033[97m',
			'\033[90m',
		)
		if metric>=0:
			color=colors[int(metric%len(colors))]
		return color

	def _color_realistic(self,point:Point):
		color='\033[92m'
		if point.terrain==TerrainType.SALTWATER:
			color='\033[94m'
		elif point.terrain==TerrainType.FRESHWATER:
			color='\033[96m'
		if point.temperature==TempType.POLAR:
			color='\033[97m'
		if point.moisture==MoistureType.WET:
			color='\033[32m'
		elif point.moisture==MoistureType.DRY:
			color='\033[93m'
		if point.elevation==ElevationType.MOUNTAINS:
			color='\033[90m'
		return color

	def _indicate_elevation(self,point:Point,land_only=True):
		char="\u2588"
		if point.elevation==ElevationType.UNDERWATER and not land_only:
			char=" "
		elif point.elevation==ElevationType.LOWLANDS:
			char="\u2591"
		elif point.elevation==ElevationType.PLAINS:
			char="\u2592"
		elif point.elevation==ElevationType.HIGHLANDS:
			char="\u2593"
		return char

	def _indicate_tile_distance_from_land(self,point:Point):
		return str(int(point.distance_from_land/self.__map.precision)) if point.distance_from_land/self.__map.precision<10 else 'X'

	def _indicate_latitudinal_wind(self,point:Point):
		char="X"
		if point.prevailing_wind==LatitudinalWindType.NORTH:
			char="\u2191"
		elif point.prevailing_wind==LatitudinalWindType.NORTHEAST:
			char="\u2197"
		elif point.prevailing_wind==LatitudinalWindType.EAST:
			char="\u2192"
		elif point.prevailing_wind==LatitudinalWindType.SOUTHEAST:
			char="\u2198"
		elif point.prevailing_wind==LatitudinalWindType.SOUTH:
			char="\u2193"
		elif point.prevailing_wind==LatitudinalWindType.SOUTHWEST:
			char="\u2199"
		elif point.prevailing_wind==LatitudinalWindType.WEST:
			char="\u2190"
		elif point.prevailing_wind==LatitudinalWindType.NORTHWEST:
			char="\u2196"
		elif point.prevailing_wind==LatitudinalWindType.DOLDRUMS:
			char="\u2b59"

		return char

	def _indicate_longitudinal_wind(self,point:Point):
		char="X"
		if point.oscillating_wind==LongitudinalWindType.NORTH:
			char="\u2191"
		elif point.oscillating_wind==LongitudinalWindType.NORTHEAST:
			char="\u2197"
		elif point.oscillating_wind==LongitudinalWindType.EAST:
			char="\u2192"
		elif point.oscillating_wind==LongitudinalWindType.SOUTHEAST:
			char="\u2198"
		elif point.oscillating_wind==LongitudinalWindType.SOUTH:
			char="\u2193"
		elif point.oscillating_wind==LongitudinalWindType.SOUTHWEST:
			char="\u2199"
		elif point.oscillating_wind==LongitudinalWindType.WEST:
			char="\u2190"
		elif point.oscillating_wind==LongitudinalWindType.NORTHWEST:
			char="\u2196"
		elif point.oscillating_wind==LongitudinalWindType.DOLDRUMS:
			char="\u2b59"

		return char

	def _indicate_tile_distance_from_saltwater(self,point:Point):
		return str(int(point.distance_from_saltwater/self.__map.precision)) if point.distance_from_saltwater/self.__map.precision<10 else 'X'

	def _indicate_tile_distance_from_freshwater(self,point:Point):
		return str(int(point.distance_from_freshwater/self.__map.precision)) if point.distance_from_freshwater/self.__map.precision<10 else 'X'

	def _indicate_none(self):
		return "\u2588"

	def display(self,color_type:str="realistic",indicator_type:str="elevation")->None:
		minlat=len(str(abs(self.__map.get_latitude_start())))
		maxlat=len(str(abs(self.__map.get_latitude_end())))
		preclat=minlat if minlat>maxlat else maxlat
		minlon=len(str(abs(self.__map.get_longitude_start())))
		maxlon=len(str(abs(self.__map.get_longitude_end())))
		preclon=minlon if minlon>maxlon else maxlon
		for y in frange(self.__map.get_latitude_start(), self.__map.get_latitude_end()+self.__map.precision, self.__map.precision):
			print('\033[0m'+"{:0>{}.0f}".format(abs(y),preclat)+" ",end="")
			for x in frange(
					self.__map.get_longitude_start(),
					self.__map.get_longitude_end()+self.__map.precision,
					self.__map.precision):
				if y not in (self.__map.get_latitude_start(),self.__map.get_latitude_end()) or x==self.__map.get_longitude_start():
					point:Point=self.__map[x,y]
					if color_type=="realistic":
						color=self._color_realistic(point)
					elif color_type=="temperature":
						color=self._color_temp(point)
					elif color_type=="nucleation":
						color=self._color_identify_metric(point.nucleation_point)
					elif color_type=="continents":
						color=self._color_identify_metric(point.continent)
					elif color_type=="basins":
						color=self._color_identify_metric(point.basin)
					elif color_type=="moisture":
						color=self._color_moisture(point)
					else:
						raise Exception("Unknown coloring type: "+color_type)
					if indicator_type=="elevation":
						char=self._indicate_elevation(point,False)
					elif indicator_type=="distance_from_land":
						char=self._indicate_tile_distance_from_land(point)
					elif indicator_type=="distance_from_saltwater":
						char=self._indicate_tile_distance_from_saltwater(point)
					elif indicator_type=="distance_from_freshwater":
						char=self._indicate_tile_distance_from_freshwater(point)
					elif indicator_type=="prevailing_wind":
						char=self._indicate_latitudinal_wind(point)
					elif indicator_type=="oscillating_wind":
						char=self._indicate_longitudinal_wind(point)
					elif indicator_type=="none":
						char=self._indicate_none()
					else:
						raise Exception("Unknown indication type: "+indicator_type)
					print(color+char,end='')
			print()

		lonarr={}
		for x in frange(
				self.__map.get_longitude_start(),
				self.__map.get_longitude_end()+self.__map.precision,
				self.__map.precision):
			lonarr[x]="{:0>{}.0f}".format(abs(x),preclon)
		for char in range(preclon):
			print('\033[0m   ',end="")
			for x in frange(
					self.__map.get_longitude_start(),
					self.__map.get_longitude_end()+self.__map.precision,
					self.__map.precision):
				print(lonarr[x][char],end="")
			print()
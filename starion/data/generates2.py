# cython: language_level=3
from data import *
from data.select import Selector
from data.types import *
from data.map import Map,Point
import random

class GenerateS2:
	"""
	Distance and elevation, metrics
	"""
	def __init__(self,generator:'MapGenerator',map:Map,selector:Selector):
		self.__generator:'MapGenerator'=generator
		self.__map:Map=map
		self.__selector:Selector=selector

	def run(self):

		print("Calculating distances...")
		self.__calculate_distances()

		print("Identifying continents...")
		self.__identify_continents()

		print("Identifying basins...")
		self.__identify_basins()

		self.__compute_elevation()

	def __compute_elevation(self):
		for point in self.__map.get_points(TerrainType.SALTWATER):
			point.elevation=ElevationType.UNDERWATER
		for point in self.__map.get_points(TerrainType.FRESHWATER):
			# this should really be one lower than the lowest surrounding elevation... but do we actually care?
			point.elevation=ElevationType.LOWLANDS
		for point in self.__map.get_points(TerrainType.LAND):
			point.elevation=ElevationType.PLAINS
		print("Raising highlands...")
		self.__compute_elevation_highlands()
		print("Lowering lowlands...")
		self.__compute_elevation_lowlands()
		print("Raising mountains...")
		self.__compute_elevation_mountains()

	def __compute_elevation_mountains(self):
		last_mountain: Point=self.__selector.select_random_land_highlands()
		rejection_counter=0
		while len(self.__map.get_points(ElevationType.MOUNTAINS))/(
				len(self.__map.get_points(TerrainType.LAND)))<self.__map.mountain_ratio:
			if random.random()>self.__map.mountain_size_multiplier:
				while last_mountain.elevation!=ElevationType.HIGHLANDS and rejection_counter<len(
						self.__map.get_points(TerrainType.LAND)):
					rejection_counter+=1
					last_mountain=self.__selector.select_random_land_highlands()
				while last_mountain.elevation!=ElevationType.HIGHLANDS and last_mountain.elevation!=ElevationType.PLAINS:
					last_mountain=self.__selector.select_random_land_noncoast()
			else:
				while last_mountain.elevation!=ElevationType.HIGHLANDS or last_mountain.terrain!=TerrainType.LAND:
					last_mountain=self.__selector.select_random_walk(last_mountain)
			last_mountain.elevation=ElevationType.MOUNTAINS

	def __compute_elevation_lowlands(self):
		last_lowland: Point=self.__selector.select_random_land_coast()
		rejection_counter=0
		while len(self.__map.get_points(ElevationType.LOWLANDS))/(
				len(self.__map.get_points(TerrainType.LAND))+0.0)<self.__map.lowlands_ratio:
			if random.random()>self.__map.lowlands_size_multiplier:
				while last_lowland.elevation==ElevationType.LOWLANDS or last_lowland.elevation==ElevationType.HIGHLANDS and rejection_counter<len(
						self.__map.get_points(TerrainType.LAND)):
					rejection_counter+=1
					last_lowland=self.__selector.select_random_land_coast()
				while last_lowland.elevation==ElevationType.HIGHLANDS or last_lowland.elevation==ElevationType.LOWLANDS:
					last_lowland=self.__selector.select_random_land_noncoast()
			else:
				while last_lowland.elevation==ElevationType.LOWLANDS or last_lowland.elevation==ElevationType.HIGHLANDS or last_lowland.terrain!=TerrainType.LAND:
					last_lowland=self.__selector.select_random_walk(last_lowland)
			last_lowland.elevation=ElevationType.LOWLANDS

	def __compute_elevation_highlands(self):
		last_highland: Point=self.__selector.select_random_land_noncoast()
		rejection_counter=0
		while len(self.__map.get_points(ElevationType.HIGHLANDS))/(
				len(self.__map.get_points(TerrainType.LAND))+0.0)<self.__map.highlands_ratio+self.__map.mountain_ratio:
			if random.random()>self.__map.highlands_size_multiplier:
				while last_highland.elevation==ElevationType.HIGHLANDS and rejection_counter<len(
						self.__map.get_points(TerrainType.LAND)):
					rejection_counter+=1
					last_highland=self.__selector.select_random_land_noncoast()
				while last_highland.elevation==ElevationType.HIGHLANDS:
					last_highland=self.__selector.select_random_land_coast()
			else:
				while last_highland.elevation==ElevationType.HIGHLANDS or last_highland.terrain!=TerrainType.LAND:
					last_highland=self.__selector.select_random_walk(last_highland)
			last_highland.elevation=ElevationType.HIGHLANDS

	def __identify_basins(self):
		basin_grouping_points=[]
		basin_count=0
		sea_points=[x for x in self.__map.get_points(TerrainType.SALTWATER)]
		sea_points.sort(key=lambda x:x.distance_from_land*100000+x.X*1000+x.Y,reverse=True)
		for point in sea_points:
			if point.is_waterlocked:
				matches=tuple(x.basin for x in point.neighbors if x in basin_grouping_points)
				basin_grouping_points.append(point)
				if len(matches)==0:
					point.basin=basin_count
					basin_count+=1
				else:
					chosen_basin=Counter(matches).most_common(1)[0][0]
					point.basin=chosen_basin
		basins=tuple(set(tuple(x.basin for x in self.__map.get_points(TerrainType.SALTWATER) if x.basin!=-1)))
		dict_basins={basins[x]:x for x in range(len(basins))}
		map_basins={}
		for point in (x for x in self.__map.get_points(TerrainType.SALTWATER) if x.basin!=-1):
			point.basin=dict_basins[point.basin]
			if point.basin not in map_basins:
				map_basins[point.basin]=[]
			map_basins[point.basin].append(point)
		toset_basins=[]
		for point in self.__map.get_points(TerrainType.SALTWATER):
			if point.basin==-1:
				toset_basins.append(point)
		changed=True
		while changed:
			changed=False
			for point in toset_basins:
				if point.basin==-1:
					basins=tuple(x.basin for x in point.neighbors if x.basin!=-1)
					if len(basins)>0:
						changed=True
						point.basin=Counter(basins).most_common(1)[0][0]
						map_basins[point.basin].append(point)
		for basin_index,basin_points in map_basins.items():
			map_neighbors={}
			for point in basin_points:
				for neighbor in point.neighbors:
					if neighbor.basin!=basin_index and neighbor.basin!=-1:
						if neighbor.basin not in map_neighbors:
							map_neighbors[neighbor.basin]=[]
						if neighbor not in map_neighbors[neighbor.basin]:
							map_neighbors[neighbor.basin].append(neighbor)
			largest_interface=0
			interfacing_basin=-1
			for neighboring_basin,interface_points in map_neighbors.items():
				interface_count=len(interface_points)
				if interface_count>largest_interface:
					largest_interface=interface_count
					interfacing_basin=neighboring_basin
			if interfacing_basin!=-1 and len(basin_points)<largest_interface*20+900/self.__map.precision or len(basin_points)<450/self.__map.precision:
				for remap_point in basin_points:
					remap_point.basin=interfacing_basin
		basins=tuple(set(tuple(x.basin for x in self.__map.get_points(TerrainType.SALTWATER) if x.basin!=-1)))
		dict_basins={basins[x]:x for x in range(len(basins))}
		map_basins={}
		for point in (x for x in self.__map.get_points(TerrainType.SALTWATER) if x.basin!=-1):
			point.basin=dict_basins[point.basin]
			if point.basin not in map_basins:
				map_basins[point.basin]=[]
			map_basins[point.basin].append(point)

	def __identify_continents(self):
		continent_grouping_points=[]
		continent_count=0
		# sort by distance from coast so that continents build up from their center points
		land_points=[x for x in self.__map.get_points(TerrainType.LAND)]
		land_points.sort(key=lambda x:x.distance_from_saltwater*100000+x.X*1000+x.Y,reverse=True)
		for point in land_points:
			# only test non-coastal points
			if point.is_landlocked:
				# if any of the neighbors have already been assigned to a continent
				matches=tuple(x.continent for x in point.neighbors if x in continent_grouping_points)
				continent_grouping_points.append(point)
				if len(matches)==0:
					# if not, new continent
					point.continent=continent_count
					continent_count+=1
				else:
					# else, use previous continent
					chosen_continent=Counter(matches).most_common(1)[0][0]
					point.continent=chosen_continent
		# renumber and compile a map of continent points
		continents=tuple(set(tuple(x.continent for x in self.__map.get_points(TerrainType.LAND) if x.continent!=-1)))
		dict_continents={continents[x]:x for x in range(len(continents))}
		map_continents={}
		for point in (x for x in self.__map.get_points(TerrainType.LAND) if x.continent!=-1):
			point.continent=dict_continents[point.continent]
			if point.continent not in map_continents:
				map_continents[point.continent]=[]
			map_continents[point.continent].append(point)
		# find coasts that have not been set yet
		toset_continents=[]
		for point in self.__map.get_points(TerrainType.LAND):
			if point.continent==-1:
				toset_continents.append(point)
		# set all coasts
		changed=True
		while changed:
			changed=False
			for point in toset_continents:
				if point.continent==-1:
					continents=tuple(x.continent for x in point.neighbors if x.continent!=-1)
					if len(continents)>0:
						changed=True
						point.continent=Counter(continents).most_common(1)[0][0]
						map_continents[point.continent].append(point)
		# identify extraneous continents and combine
		for continent_index,continent_points in map_continents.items():
			map_neighbors={}
			# identify continents neighboring this continent
			for point in continent_points:
				for neighbor in point.neighbors:
					if neighbor.continent!=continent_index and neighbor.continent!=-1:
						if neighbor.continent not in map_neighbors:
							map_neighbors[neighbor.continent]=[]
						if neighbor not in map_neighbors[neighbor.continent]:
							map_neighbors[neighbor.continent].append(neighbor)
			# find the neighbor with the largest interface (border)
			largest_interface=0
			interfacing_continent=-1
			for neighboring_continent,interface_points in map_neighbors.items():
				interface_count=len(interface_points)
				if interface_count>largest_interface:
					largest_interface=interface_count
					interfacing_continent=neighboring_continent
			# if interface is too large or continent is too small, combine
			if interfacing_continent!=-1 and len(continent_points)<largest_interface*10+300/self.__map.precision or len(continent_points)<150/self.__map.precision:
				for remap_point in continent_points:
					remap_point.continent=interfacing_continent
		# renumber and compile a map of continent points (again)
		continents=tuple(set(tuple(x.continent for x in self.__map.get_points(TerrainType.LAND) if x.continent!=-1)))
		dict_continents={continents[x]:x for x in range(len(continents))}
		map_continents={}
		for point in (x for x in self.__map.get_points(TerrainType.LAND) if x.continent!=-1):
			point.continent=dict_continents[point.continent]
			if point.continent not in map_continents:
				map_continents[point.continent]=[]
			map_continents[point.continent].append(point)

	def __calculate_distances(self):
		self.__generator.initialize_progress(self.__map.get_magnitude()*3)
		points_land: Tuple[Point,...]=self.__map.get_points(TerrainType.LAND)
		points_saltwater: Tuple[Point,...]=self.__map.get_points(TerrainType.SALTWATER)
		points_freshwater: Tuple[Point,...]=self.__map.get_points(TerrainType.FRESHWATER)
		for point in points_land:
			self.__generator.update_progress(1)
			point.distance_from_land=0
		for point in points_saltwater:
			self.__generator.update_progress(1)
			point.distance_from_saltwater=0
		for point in points_freshwater:
			self.__generator.update_progress(1)
			point.distance_from_freshwater=0
		toset_land: List[Point,...]=list(points_freshwater+points_saltwater)
		toset_saltwater: List[Point,...]=list(points_freshwater+points_land)
		toset_freshwater: List[Point,...]=list(points_saltwater+points_land)
		generation: float=0
		while len(toset_land)>0:
			for point in toset_land:
				lowest_value=float("inf")
				lowest_neighbor=None
				for neighbor in point.neighbors:
					if neighbor.distance_from_land>=0 and neighbor.distance_from_land<=generation and neighbor.distance_from_land<lowest_value:
						lowest_neighbor=neighbor
						lowest_value=neighbor.distance_from_land
				if lowest_neighbor is None:
					continue
				point.distance_from_land=lowest_value+self.__map.compute_real_distance(lowest_neighbor,point)
				self.__generator.update_progress(1)
				toset_land.remove(point)
			generation+=0.1
		generation=0
		while len(toset_saltwater)>0:
			for point in toset_saltwater:
				lowest_value=float("inf")
				lowest_neighbor=None
				for neighbor in point.neighbors:
					if neighbor.distance_from_saltwater>=0 and neighbor.distance_from_saltwater<=generation and neighbor.distance_from_saltwater<lowest_value:
						lowest_neighbor=neighbor
						lowest_value=neighbor.distance_from_saltwater
				if lowest_neighbor is None:
					continue
				point.distance_from_saltwater=lowest_value+self.__map.compute_real_distance(lowest_neighbor,point)
				self.__generator.update_progress(1)
				toset_saltwater.remove(point)
			generation+=0.1
		generation=0
		while len(toset_freshwater)>0:
			for point in toset_freshwater:
				lowest_value=float("inf")
				lowest_neighbor=None
				for neighbor in point.neighbors:
					if neighbor.distance_from_freshwater>=0 and neighbor.distance_from_freshwater<=generation and neighbor.distance_from_freshwater<lowest_value:
						lowest_neighbor=neighbor
						lowest_value=neighbor.distance_from_freshwater
				if lowest_neighbor is None:
					continue
				point.distance_from_freshwater=lowest_value+self.__map.compute_real_distance(lowest_neighbor,point)
				self.__generator.update_progress(1)
				toset_freshwater.remove(point)
			generation+=0.1
		self.__generator.clear_progress()
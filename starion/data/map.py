# cython: language_level=3
from typing import *
from data import frange
from data.types import *
import math
# These two classes are pretty closely tied together

class Point:
	def __get_X(self) -> float:
		return self.__x

	X=property(__get_X)

	def __get_Y(self) -> float:
		return self.__y

	Y=property(__get_Y)

	def recenter(self,rightward_shift: int) -> None:
		new_longitude=self.__x-rightward_shift
		new_tup=self.__map.validate_coordinate((new_longitude,self.__y))
		self.__x=new_tup[0]

	def __get_neighbors(self) -> Tuple['Point',...]:
		# ordering is important, so we don't call the other two methods; they are nested into each other, not concatenated
		if self.is_pole:
			return self.__map.get_polar_ring_points(self)
		else:
			# performance optimizations
			precision=self.__map.precision
			x=self.X
			xp=x+precision
			xm=x-precision
			y=self.Y
			yp=y+precision
			ym=y-precision
			m=self.__map
			# e, se, s, sw, w, nw, n, ne
			return m[xp,y],m[xp,yp],m[x,yp],m[xm,yp],m[xm,y],m[xm,ym],m[x,ym],m[xp,ym]

	neighbors=property(__get_neighbors)

	def __get_longitudinal_dimension(self):
		return self.__map.precision*math.cos(self.Y/180.0*math.pi)

	longitudinal_dimension=property(__get_longitudinal_dimension)

	def __get_latitudinal_dimension(self):
		return self.__map.precision

	latitudinal_dimension=property(__get_latitudinal_dimension)

	def __get_area(self):
		# slight simplification, but is correct as the limit of map.precision approaches 0, and not far off for precisions <10
		return self.__get_latitudinal_dimension()*self.__get_longitudinal_dimension()

	area=property(__get_area)

	def __get_is_pole(self) -> bool:
		return self.__map.get_latitude_start()==self.Y or self.__map.get_latitude_end()==self.Y

	is_pole=property(__get_is_pole)

	def __get_is_landlocked(self) -> bool:
		return len(tuple(x for x in self.neighbors if x.terrain!=TerrainType.LAND))==0

	is_landlocked=property(__get_is_landlocked)

	def __get_is_waterlocked(self) -> bool:
		return len(tuple(x for x in self.neighbors if x.terrain==TerrainType.LAND))==0

	is_waterlocked=property(__get_is_waterlocked)

	def __get_latitudinal_hemisphere(self)->DirectionType:
		return DirectionType.NORTH if self.Y<self.__map.get_latitude_midpoint() else DirectionType.SOUTH if self.Y>self.__map.get_latitude_midpoint() else DirectionType.INCOGNITUM

	latitudinal_hemisphere=property(__get_latitudinal_hemisphere)

	def __get_longitudinal_hemisphere(self)->DirectionType:
		return DirectionType.EAST if self.X<self.__map.get_longitude_midpoint() else DirectionType.WEST if self.X>self.__map.get_longitude_midpoint() else DirectionType.INCOGNITUM

	longitudinal_hemisphere=property(__get_longitudinal_hemisphere)

	def __repr__(self):
		return "<"+str(self.X)+","+str(self.Y)+">"

	def __get_terrain(self):
		return self.__terrain

	def __set_terrain(self,value: TerrainType):
		self.__map.update_point_cache(TerrainType,self,self.__terrain,value)
		self.__terrain=value

	terrain: TerrainType=property(__get_terrain,__set_terrain)

	def __get_temp(self):
		return self.__temperature

	def __set_temp(self,value: TempType):
		self.__map.update_point_cache(TempType,self,self.__temperature,value)
		self.__temperature=value

	temperature: TempType=property(__get_temp,__set_temp)

	def __get_moisture(self):
		return self.__moisture

	def __set_moisture(self,value: MoistureType):
		self.__map.update_point_cache(MoistureType,self,self.__moisture,value)
		self.__moisture=value

	moisture: MoistureType=property(__get_moisture,__set_moisture)

	def __get_elevation(self):
		return self.__elevation

	def __set_elevation(self,value: ElevationType):
		self.__map.update_point_cache(ElevationType,self,self.__elevation,value)
		self.__elevation=value

	elevation: ElevationType=property(__get_elevation,__set_elevation)

	def __get_climate(self):
		return self.__climate

	def __set_climate(self,value: ClimateType):
		self.__map.update_point_cache(ClimateType,self,self.__climate,value)
		self.__climate=value

	climate: ClimateType=property(__get_climate,__set_climate)

	def __get_prevailing_wind(self):
		return self.__prevailing_wind

	def __set_prevailing_wind(self,value: LatitudinalWindType):
		self.__map.update_point_cache(LatitudinalWindType,self,self.__prevailing_wind,value)
		self.__prevailing_wind=value

	prevailing_wind: LatitudinalWindType=property(__get_prevailing_wind,__set_prevailing_wind)

	def __get_oscillating_wind(self):
		return self.__oscillating_wind

	def __set_oscillating_wind(self,value: LongitudinalWindType):
		self.__map.update_point_cache(LongitudinalWindType,self,self.__oscillating_wind,value)
		self.__oscillating_wind=value

	oscillating_wind: LongitudinalWindType=property(__get_oscillating_wind,__set_oscillating_wind)

	def __init__(self,coords: Tuple[float,float],map: 'Map'):
		self.__map=map

		self.__terrain: TerrainType=None
		self.__temperature: TempType=None
		self.__moisture: MoistureType=None
		self.__elevation: ElevationType=None
		self.__climate: ClimateType=None
		self.__prevailing_wind: LatitudinalWindType=None
		self.__oscillating_wind: LongitudinalWindType=None
		# important to set all these twice so that the private variables above are defined in init but the point cache is updated below
		self.terrain: TerrainType=TerrainType.INCOGNITUM
		self.temperature: TempType=TempType.INCOGNITUM
		self.moisture: MoistureType=MoistureType.INCOGNITUM
		self.elevation: ElevationType=ElevationType.INCOGNITUM
		self.climate: ClimateType=ClimateType.INCOGNITUM
		self.prevailing_wind: LatitudinalWindType=LatitudinalWindType.INCOGNITUM

		self.distance_from_land: float=-1
		self.distance_from_saltwater: float=-1
		self.distance_from_freshwater: float=-1
		self.nucleation_point: int=-1
		self.continent: int=-1
		self.basin: int=-1
		self.ancestor:Point=None # this is purely for calculations to use, don't rely on this meaning anything
		self.age:int=0 # same as above
		self.weight:int=0 # same as above
		self.__x=coords[0]
		self.__y=coords[1]


class Map:
	def __init__(self):
		self.__points: Dict[float,Dict[float,Point]]={}
		self.__refs: Dict[Point,Tuple[float,float]]={}
		self.__genset_x: List[float]=[]
		self.__genset_y: List[float]=[]
		# default args
		self.precision=5 # size of cells in degrees. 5 is about the smallest that performs "fast". Directly affects the "resolution" of the resulting map.
		self.__set_longitude(-180,180) # longitude range. Earth-like planets in a euclidean spheroid coordinate system use (-180,180)
		self.__set_latitude(-90,90) # latitude range. Earth-like planets in a euclidean spheroid coordinate system use (-90,90)
		self.land_sea_ratio=0.3 # approximate ratio of land to sea. For Earth, this is around 0.3 (that is, 30% land, 70% water, by surface area)
		self.island_continents_ratio=0.07 # approximate ratio of land in continents to land in islands. For earth, this is around 0.07 (that is, about 7% of Earth's total landmass is part of an identifiable "island" rather than a continental "mainland").
		self.continent_nucleation_points=13  # number of points to build continents from. Note that this does not represent the number of continents, e.g. a big continent like eurasia might be 'built' by four or five conjoined nucleation points. An Earth-like constitution seems to be around 13, smaller numbers give increasingly pangaea-like and blobby worlds, while larger numbers give an archipelago.
		self.continent_multiplier=0.7 # ratio of continent nucleation sizes, in the range (0,1). Smaller number gives more uniformly sized continent masses overall. Large numbers will give decreasing continent mass sizes. Remember that depending on how the nucleation points align, several continent masses may end up joining into one single "continent" for visual purposes. 0.7 seems to give an earth-like appearance.
		self.continent_passes=((0.75,0.25),(0.5,0.5),(0.25,0.75),(0,1)) # steps to use in building up continents. Each pass has args (variance, fraction). Variance specifies how "stringy" the built-up shapes are, with 1 being long strings and 0 being round shapes. Fraction specifies how much of the landmass to build up this way before stopping, and should increase to 1 over all passes. You probably also want variance to decrease to near 0 over all passes, but this is not required depending on the type of landmass you're looking for.
		self.continent_cleanup_resolution=10 # small saltwater lakes will be cleaned up. This size is without regard to precision; a precision of 1 allows lakes to be larger than 10 cells, while a precision of 5 allows lakes to be larger than 2 cells.
		self.highlands_ratio=0.2 # ratio of highlands to other terrain types. INCLUDES MOUNTAINS. Don't make the mountain ratio larger than the highlands ratio.
		self.highlands_size_multiplier=0.75 # determines the size of highlands regions. Low numbers give many small regions, high numbers give few large regions.
		self.mountain_ratio=0.1 # ratio of mountains to other terrain types.
		self.mountain_size_multiplier=0.75 # determines the size of mountain regions. Low numbers give many small regions, high numbers give few large regions.
		self.lowlands_ratio=0.2 # ratio of lowlands to other terrain types.
		self.lowlands_size_multiplier=0.75 # determines the size of lowlands regions. Low numbers give many small regions, high numbers give few large regions.
		self.elevation_temperature_factor=3 # how much elevation influences temperature. Higher numbers give more pronounced "cold continental mountain range" and "warm tropical coast" effects.
		self.temperature_multiplier=0.5 # temperature band skewing. Small numbers will give a snowball planet with a small strip of warm temperatures near the equator, large numbers will give a tropical planet.
		self.temperature_skewrate=0.05 # temperature localized skewing. Large numbers will pull cold temperatures further south into the interior of continents and warm temperatures further north in ocean areas.
		self.temperature_skewmult=0.25 # temperature localized skewing multiplier. Large numbers will cause the skew rate to increase more rapidly as you move deeper inland/into sea areas, small numbers will make this rate-of-change more gradual.
		self.temperature_draw_neighborhood=5 # temperature drawdown/drawup due to topographic features will examine neighboring X cells. Drawing can be turned off by entering 9 (more cells than possible to have neighboring in a rectangular grid)
		self.wind_multiplier=0.5 # wind band skewing. Small numbers will give a planet with a permanent stormfront encircling the equator and calm areas in the polar regions, large numbers will give a more north-south wind orientation than earth.
		self.wind_skewrate=0.1 # wind localized skewing. Large numbers will pull winds further north into the interior of continents and further south in ocean areas.
		self.wind_skewmult=0.1 # wind localized skewing multiplier. Large numbers will cause the skew rate to increase more rapidly as you move deeper inland/into sea areas, small numbers will make this rate-of-change more gradual.
		self.climate_multiplier=0.5  # TODO
		self.moisture_multiplier=0.5  # TODO
		self.freshwater_multiplier=0.01 # ratio of liquid freshwater to saltwater. Earth's is 0.01 (3% of the water on the surface of the planet is freshwater, but 2% of that is polar icecaps)
		self.island_size_multiplier=0.75 # island size relationship. Small numbers give many small islands, large numbers give a few bigger ones.
		self.lake_size_multiplier=0.75 # lake size relationship. Small numbers give many small freshwater lakes, large numbers give a few bigger ones.

		self.__point_cache: Dict[
			Type,Dict[Union[TerrainType,TempType,MoistureType,ElevationType,ClimateType,LatitudinalWindType],List[Point,...]]]={}
		for x in (TerrainType,TempType,MoistureType,ElevationType,ClimateType,LatitudinalWindType):
			self.__point_cache[x]={}
			for y in (z.value for z in x):
				self.__point_cache[x][y]=[]

	def __getitem__(self,item: Tuple[float,float]) -> Point:
		coord=self.validate_coordinate(item)
		return self.__points[coord[1]][coord[0]]

	def validate_coordinate(self,item: Tuple[float,float]):
		# this method is called a lot, so there are performance optimizations in how it's written
		# x and y are any numbers
		# x must be moved into the range [lonstart,lonend]
		# y must be moved into the range [latstart,latend]
		# x must be moved in steps of lonspan
		# y must be moved in steps of latspan and then inverted
		# x must be moved in steps of lonspan/2 whenever y is moved
		# x must be moved after y is in its final position
		x=item[0]
		y=item[1]
		while y<self.__lats:
			y+=self.__latp
			y*=-1
			x+=self.__lonp/2.0
		while y>self.__late:
			y-=self.__latp
			y*=-1
			x+=self.__lonp/2.0
		if y==self.__lats or y==self.__late:
			x=self.__lons
		while x<self.__lons:
			x+=self.__lonp
		while x>self.__lone:
			x-=self.__lonp
		return x,y

	def initialize(self):
		for x in frange(
				self.__lons,
				self.__lone+self.precision,
				self.precision):
			self.__genset_x.append(x)
			# don't want either -90 or 90 to overlap other points at the north/south pole, so skip the last precision
			for y in frange(self.__lats,self.__late+self.precision,self.precision):
				if x==self.__lons:
					self.__genset_y.append(y)
				point: Point=Point((x,y),self)
				tup: Tuple[float,float]=(x,y)
				if tup[1] not in self.__points:
					self.__points[tup[1]]={}
				self.__points[tup[1]][tup[0]]=point
				self.__refs[point]=tup
		# add poles
		self.__genset_y.append(self.__lats)
		self.__points[self.__lats][self.__lons]=Point((self.__lons,self.__lats),
			self)
		self.__refs[self.point((self.__lons,self.__lats))]=(
		self.__lons,self.__lats)
		self.__genset_y.append(self.__late)
		self.__points[self.__late][self.__lons]=Point((self.__lons,self.__late),
			self)
		self.__refs[self.point((self.__lons,self.__late))]=(
		self.__lons,self.__late)

	def ref(self,point: Point):
		return self.__refs[point]

	def point(self,ref: Tuple[float,float]):
		return self.__points[ref[1]][ref[0]]

	def equivalent_latitude(self,point: Point,lat: float,multiplier: float) -> bool:
		return point.Y>lat*2*multiplier or point.Y<-1*lat*2*multiplier

	def get_longitude_span(self):
		return self.__lonp

	def get_latitude_span(self):
		return self.__latp

	def get_longitude_midpoint(self):
		return self.__lonm

	def get_latitude_midpoint(self):
		return self.__latm

	def get_magnitude(self):
		return len(self.__refs)

	def aggregate_area(self,points: Union[Tuple[Point,...],List[Point]]) -> float:
		return sum(x.area for x in points)

	def compute_tile_distance_x(self,a: Point,b: Point):
		test_1=a.X
		test_2=a.X
		tile_distance=0
		while test_1!=b.X and test_2!=b.X:
			tile_distance+=1
			test_1=self.validate_coordinate((test_1-self.precision,0))[0]
			test_2=self.validate_coordinate((test_2+self.precision,0))[0]
		return tile_distance

	def compute_tile_distance_y(self,a: Point,b: Point):
		test_1=a.Y
		test_2=a.Y
		tile_distance=0
		while test_1!=b.Y and test_2!=b.Y:
			tile_distance+=1
			test_1=self.validate_coordinate((0,test_1-self.precision))[1]
			test_2=self.validate_coordinate((0,test_2+self.precision))[1]
		return tile_distance

	def compute_real_distance(self,a: Point,b: Point):
		tile_distance_x=self.compute_tile_distance_x(a,b)
		tile_distance_y=self.compute_tile_distance_y(a,b)
		return math.sqrt(math.pow(tile_distance_x*min(a.longitudinal_dimension,b.longitudinal_dimension),2)+math.pow(
			tile_distance_y*min(
				a.latitudinal_dimension,b.latitudinal_dimension),2))

	def __set_longitude(self,lons,lone):
		# don't want -180 and 180 to overlap each other, but want the points at that location to have the longitude 180, not -180, so add precision to both if the east end is negative (if it is 0 we do want that point to be 0, not 360, and if it is positive idkwtf you're doing)
		self.__lons=lons+(self.precision if lons<0 else 0)
		self.__lone=lone+(self.precision if lons<0 else 0)-self.precision
		self.__lonp=self.__lone-self.__lons+self.precision
		self.__lonm=abs(self.__lons)-abs(self.__lone)
	
	def __set_latitude(self,lats,late):
		self.__lats=lats
		self.__late=late
		self.__latp=self.__late-self.__lats
		self.__latm=abs(self.__lats)-abs(self.__late)

	def get_latitude_start(self):
		return self.__lats

	def get_latitude_end(self):
		return self.__late

	def get_longitude_end(self):
		return self.__lone

	def get_longitude_start(self):
		return self.__lons

	def get_latitude_cells(self):
		return tuple(self.__genset_x)

	def get_longitude_cells(self):
		return tuple(self.__genset_y)

	def update_point_cache(self,cache: Type,point: Point,old,new):
		if old is not None:
			self.__point_cache[cache][old].remove(point)
		if new is not None:
			self.__point_cache[cache][new].append(point)

	def get_points(self,criterion=None) -> Tuple[Point,...]:
		if criterion is None:
			return tuple(y for x in self.__points.values() for y in tuple(x.values()))
		else:
			return tuple(self.__point_cache[type(criterion)][criterion])

	def get_polar_ring_points(self,pole: Point=None) -> Tuple[Point,...]:
		tup=()
		if pole is None or pole.Y==self.__lats:
			tup+=tuple(self.__points[self.__lats+self.precision].values())
		if pole is None or pole.Y==self.__late:
			tup+=tuple(self.__points[self.__late-self.precision].values())
		if pole is not None and pole.Y!=self.__lats and pole.Y!=self.__late:
			raise ValueError("Not a pole")
		return tup

	def recenter(self,rightward_shift: int) -> None:
		recentered_points: Dict[float,Dict[float,Point]]={}
		recentered_refs: Dict[Point,Tuple[float,float]]={}
		for lat,latdict in self.__points.items():
			for lon,point in latdict.items():
				new_longitude=lon-rightward_shift
				new_tup=self.validate_coordinate((new_longitude,lat))
				if new_tup[1] not in recentered_points:
					recentered_points[new_tup[1]]={}
				recentered_points[new_tup[1]][new_tup[0]]=point
				recentered_refs[point]=new_tup
				point.recenter(rightward_shift)
		self.__points=recentered_points
		self.__refs=recentered_refs
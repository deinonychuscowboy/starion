STARION
=======

Starion is a cartography generation library. For generating plausible-looking
maps of fictional worlds. In python! Because I'm tired of drawing endless
maps of my fictional universes and trying to make them "look right" when in
fact I can enumerate a lot of the rules I'm using to do so.

Pretty basic right now. Generates an equirectangular map, but does take into
account real non-euclidean distances, so it should look vaguely correct if
you put it onto a globe and not have horribly smooshed geography near the 
poles.

Will also eventually attempt to guess realistic weather patterns for the
purposes of locating deserts and rainforests.

Named after the Mitsubishi Starion because I dgaf anymore about thinking up
good names for my projects.

You can run the native map generation with just `starion/__init__.py`, but you
may wish to first compile the data libraries to C modules with Cython
(`pip3 install cython`), by running `./setup.py build_ext --inplace`. This will
improve execution times by around 50%. Starion can be pretty slow with high
precision settings (smaller than about 5 degrees per cell). 